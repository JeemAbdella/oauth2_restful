<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use AppBundle\Entity\Post;

class LoadUserData implements FixtureInterface , ContainerAwareInterface
{
    /**
    * Injection du container 
    * @var ContainerInterface
    */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
    * Chargement des données vers la base de données
    */
    public function load(ObjectManager $manager)
    {
        // Ajout de l'admin
        $userAdmin = new User(               );
        $userAdmin->setUsername('admin'      );
        $userAdmin->setPlainPassword('admin' );
        $userAdmin->setEmail('admin@mail.com');
        $userAdmin->setEnabled(true          );
        $userAdmin->addRole("ROLE_ADMIN"     );
        $manager->persist($userAdmin         );
        
        // Ajout de l'utilisateur
        $user = new User();
        $user->setPlainPassword('user'  );
        $user->setUsername('user'       );
        $user->setEmail('user@mail.com' );
        $user->setEnabled(true          );
        $user->addRole("ROLE_USER"      ); 
        $manager->persist($user         );
        
        $manager->flush();
        
        // Ajout du premier post 
        $post = new Post(            );
        $post->setTitle("Lorem ipsum");
        $post->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vel metus at mi tempus malesuada et id orci. Praesent aliquet at est ut semper. Pellentesque facilisis aliquet sapien non blandit. Sed eu enim auctor, vulputate dolor a, hendrerit nunc. Nam luctus quam dui, sit amet fringilla turpis elementum sed. Cras non lacus vitae purus molestie tristique a at tellus. Sed pretium dignissim velit malesuada laoreet. Curabitur semper tellus augue, et ornare erat imperdiet dapibus. Maecenas aliquam, ipsum at ullamcorper egestas, eros libero ultrices ex, ut porttitor ante justo vel eros. Vestibulum id nibh pretium, pellentesque nibh et, malesuada lectus. Donec id ante dignissim augue ultricies sagittis eget ac felis. Praesent quis massa rutrum, congue arcu at, eleifend lacus. Morbi non fermentum tellus. Aliquam commodo laoreet tortor, at maximus tortor pharetra ut.");
        $post->setOwner($user        );
        $manager->persist($post      );
        
        // Ajour du deuxème post 
        $post = new Post();
        $post->setTitle("Vivamus interdum eros");
        $post->setContent("Vivamus interdum eros vel ipsum viverra, a tristique ante tristique. Nulla ac dolor dolor. Pellentesque ac lectus pharetra, accumsan diam eu, porttitor enim. Nulla ac consequat urna. Cras blandit at elit sed maximus. Phasellus ut urna in neque cursus pretium vitae id dui. Duis viverra felis ut purus posuere, id tincidunt purus luctus. Suspendisse potenti. Ut pharetra ut eros sed pretium. Etiam sodales tristique dui ut aliquet. Curabitur vitae sodales nisl. Aenean lobortis rutrum enim venenatis bibendum. Etiam in erat quis nibh congue viverra.");
        $post->setOwner($user);
        $manager->persist($post);
        
        $manager->flush();
        
        $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
        $client = $clientManager->createClient();
        $client->setRedirectUris(array('http://localhost/crud/web/app_dev.php/api/public'));
        $client->setAllowedGrantTypes(array('password','token', 'authorization_code'));
        $clientManager->updateClient($client);
    }
}